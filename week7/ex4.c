#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void* myRealloc(void* ptr, int size, int old_size){
    if(ptr == NULL)
        return ptr = malloc(size);
    if(size == 0){
        free(ptr);
        return ptr;
    }
    if(size == old_size)
        return ptr;
    else if (size < old_size) {
        free(ptr+(old_size-size));
    }
    void* ptr2 = malloc(size);
    if(old_size) {
        memcpy(ptr2, ptr, old_size);
        free(ptr);
    }
    ptr = ptr2;
    return ptr;
    printf("y");
}

int main() {
	srand(time(NULL));

	printf("Enter original array size:");
	int n1=0;
	scanf("%d",&n1);

	int* a1 =(int*)myRealloc(a1, n1*sizeof(int), 0);
	int i;
	for(i=0; i<n1; i++){
		a1[i]=100;
		printf("%d ", a1[i]);
	}

	printf("\nEnter new array size: ");
	int n2=0;
	scanf("%d",&n2);

	a1 = (int*) myRealloc(a1, n2*sizeof(int), n1*sizeof(int));

	if(n2>n1)
        for(int i=n1;i<n2;i++)
            a1[i]=0;


	for(i=0; i<n2;i++){
		printf("%d ", a1[i]);
	}
	printf("\n");

	return 0;
}
