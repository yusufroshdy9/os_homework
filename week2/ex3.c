#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<string.h>

void right_angel(int n){
    int i,j;
    for(i = 1; i <= n; i++){
        for(j = 0; j < i; j++)
            printf("*");
        printf("\n");
    }
}

void isosceles(int n){
    int i,j;
    for(i = 1; i <= n; i++){
        if(i<=(n+1)/2)
            for(j = 0; j < i; j++)
                printf("*");
        else
            for(j = 0; j < n-i+1; j++)
                printf("*");
        printf("\n");
    }
}

void square(int n){
    int i,j;
    for(i = 1; i <= n; i++){
        for(j = 0; j < n; j++)
            printf("*");
        printf("\n");
    }
}

int main(int argc, char *argv[]){
    if( argc == 2 ) {
        int n = 0;
        sscanf(argv[1],"%d",&n);
        int i,j;
        for(i = 1; i <= n; i++){
            for(j = 0; j < n-i; j++)
                printf(" ");
            for(j = 0; j < 2*i-1; j++)
                printf("*");
            printf("\n");
        }

        right_angel(n);
        isosceles(n);
        square(n);
    }
    else{
        printf("Error");
    }
}
