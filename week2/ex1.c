#include<stdio.h>
#include<limits.h>
#include<float.h>

int main(){
    int i = INT_MAX;
    float f = FLT_MAX;
    double d = DBL_MAX;

    printf("size of i = %d\n",sizeof(int));
    printf("value of i = %d\n\n",i);

    printf("size of f = %d\n",sizeof(float));
    printf("value of f = %f\n\n",f);

    printf("size of d = %d\n",sizeof(double));
    printf("value of d = %f\n\n",d);
}
