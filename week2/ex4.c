#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<string.h>

void swap(int *x, int *y){
    int z = *x;
    *x = *y;
    *y = z;
}

int main(int argc, char *argv[]){
    int i,j;
    printf("Enter two numbers:\n");
    scanf("%d%d", &i, &j);
    swap(&i, &j);
    printf("i = %d, j = %d", i, j);
}
