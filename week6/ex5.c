#include<stdio.h>
#include<unistd.h>
#include<signal.h>

int main(){
    int id = fork();
    if(id == 0){
        while(1){
            printf("I'm alive\n");
            sleep(1);
        }
    }
    else{
        sleep(10);
        kill(id, SIGTERM);
    }

}
