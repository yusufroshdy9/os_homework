#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

void hop(int i){
    printf("hop hop hop\n");
    exit(0);
}

int main(){
    signal(SIGINT,hop);
    while(1);
}
