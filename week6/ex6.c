#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
#include<sys/wait.h>

int main() {
    int f[2];
    pipe(f);

    int id1 = fork();
    if(id1 != 0) {      /// in the parent
        int id2 = fork();
        if (id2 != 0) { /// in the parent
            write(f[1], &id2, sizeof(int));
            close(f[1]);
            printf("%d\n", waitpid(id2, NULL, 0));
            printf("main finished waiting 2nd child\n");
        } else {        /// in child 2
            sleep(3);
        }
    } else {    /// in child 1
        int id;
        read(f[0], &id, sizeof(int));
        sleep(2);
        kill(id, SIGSTOP);
        sleep(8);
        kill(id, SIGCONT);
    }
    exit(1);

}
