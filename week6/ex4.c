#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

void hop(int i){
    if(i == SIGSTOP)
        printf("SIGSTOP\n");///cannot catch
    else if(i == SIGKILL)
        printf("SIGKILL\n");///cannot catch
    else if(i == SIGUSR1)
        printf("SIGUSR1\n");
    else
        printf("%d\n",i);
    exit(0);
}

int main(){
    signal(SIGSTOP,hop);
    signal(SIGKILL,hop);
    signal(SIGUSR1,hop);
    while(1);
}
