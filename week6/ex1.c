#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(){
    char s[4] = "Hi\n", e[4] = "";
    int f[2];
    pipe(f);
    write(f[1], s, 4);
    read(f[0], e, 4);

    printf(e);
}
